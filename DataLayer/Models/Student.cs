﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.Models
{
    public class Student
    {
        public enum Gender
        {
            Male,
            Female,
            Other,
        }

        public int Id { get; set; }
        public string Pesel { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime Birthday { get; set; }
        public Gender StudentGender { get; set; }
        /* public int DaysOfPresence = 0;
         public int TotalHomeworkPoints = 0;*/

        public virtual ICollection<Course> Courses { get; set; }

        public Student()
        {
            Courses = new List<Course>();
        }
    }
}
﻿using System.Collections.Generic;
using DataLayer.Models;

namespace DataLayer.Interfaces
{
    public interface IStudentRepository: IGenericRepository<Student>
    {
        List<Student> GetStudentsByPesel(string pesel);
        int CountStudents();
        void UpdateStudent(Student entity);
       // bool AddStudent(Student student);
        Student GetStudentByPesel(string pesel);
    }
}
﻿using System.Collections.Generic;
using DataLayer.Models;

namespace DataLayer.Interfaces
{
    public interface ICourseRepository : IGenericRepository<Course>
    {
        List<Course> GetCourses();
        //bool AddCourse(Course course);
        bool AddStudentToCourse(string pesel, int courseId);
        bool AddHomeworkToCourse(Homework homework, int courseId);
        bool RemoveStudentFromCourse(int studentId, int courseId);
        bool AddClassDayToCourse(ClassDay classDay, int courseDtoId);

    }
}
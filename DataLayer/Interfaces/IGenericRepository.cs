﻿namespace DataLayer.Interfaces
{
    public interface IGenericRepository<T> where T : class
    {
        bool AddElement(T entity);
        bool UpdateElement(T dbModel);

    } 
}
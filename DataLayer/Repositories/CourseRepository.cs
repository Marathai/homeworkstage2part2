﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataLayer.DbContexts;
using DataLayer.Interfaces;
using DataLayer.Models;

namespace DataLayer.Repositories
{
    public class CourseRepository : GenericRepository<Course>, ICourseRepository
    {
        public List<Course> GetCourses()
        {
            List<Course> courses = null;
            using (var dbContext = new Context())
            {
                courses = dbContext.CoursesDbSet
                    .Include(c => c.Students)
                    //.Include(c => c.Homeworks)
                    //.Include(c => c.ClassDays)
                    .ToList();
            }
            return  courses;
        }

        public bool AddStudentToCourse(string pesel, int courseId)
        {
            using (var dbContext = new Context())
            {
                var courseFromDb = dbContext.CoursesDbSet
                    .Include(c => c.Students)
                    .Single(c => c.Id == courseId);

                var studentFromDb = dbContext.StudentsDbSet.Single(s => s.Pesel == pesel);

                if (courseFromDb.Students.Contains(studentFromDb))
                    return false;

                courseFromDb.Students.Add(studentFromDb);
                dbContext.SaveChanges();
                return true;
            }
        }

        public bool AddHomeworkToCourse(Homework homework, int courseId)
        {
            using(var dbContext = new Context())
            {
                var courseFromDb = dbContext.CoursesDbSet
                    .Include(c => c.Homeworks)
                    .Single(c => c.Id == courseId);

                if (courseFromDb.Homeworks.Contains(homework))
                    return false;

                courseFromDb.Homeworks.Add(homework);
                dbContext.SaveChanges();
                return true;
            }
        }

        public bool RemoveStudentFromCourse(int studentId, int courseId)
        {
            using (var dbContext = new Context())
            {
                var courseFromDb = dbContext.CoursesDbSet
                    .Include(c => c.Students)
                    .Single(c => c.Id == courseId);

                var studentFromDb = dbContext.StudentsDbSet.Single(s => s.Id == studentId);

                if (!courseFromDb.Students.Contains(studentFromDb))
                    return false;

                courseFromDb.Students.Remove(studentFromDb);
                dbContext.SaveChanges();
                return true;
            }
        }

        public bool AddClassDayToCourse(ClassDay classDay, int courseDtoId)
        {
            using (var dbContext = new Context())
            {
                var courseFromDb = dbContext.CoursesDbSet
                    .Include(c => c.ClassDays)
                    .Single(c => c.Id == courseDtoId);

                if (courseFromDb.ClassDays.Contains(classDay))
                    return false;

                courseFromDb.ClassDays.Add(classDay);
                dbContext.SaveChanges();
                return true;
            }
        }
    }
}
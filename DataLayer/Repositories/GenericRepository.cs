﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations.Model;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using DataLayer.DbContexts;
using DataLayer.Interfaces;
using DataLayer.Models;

namespace DataLayer.Repositories
{
    public abstract class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        public bool AddElement(T dbModel)
        {
            T addedEntity = null;

            using (var dbContext = new Context())
            {
                addedEntity = dbContext.Set<T>().Add(dbModel);
                dbContext.SaveChanges();
                dbContext.Entry(dbModel).Reload();
            }

            return addedEntity != null;
        }

        public bool UpdateElement(T dbModel)
        {
            using (var dbContext = new Context())
            {
                dbContext.CoursesDbSet.Attach(dbModel);
                dbContext.Entry(dbModel).State = EntityState.Modified;
                dbContext.SaveChanges();
                return true;
            }
        }

        public List<T> GetAll()
        {
            List<T> elements = null;
            using (var dbContext = new Context())
            {
                elements = dbContext.Set<T>().ToList();
            }
            return elements;
        }

    }
}
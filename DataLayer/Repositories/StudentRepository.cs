﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataLayer.DbContexts;
using DataLayer.Interfaces;
using DataLayer.Models;

namespace DataLayer.Repositories
{
    public class StudentRepository :GenericRepository<Student>, IStudentRepository
    {
        public List<Student> GetStudentsByPesel(string pesel)
        {
            List<Student> students = null;          
            using (var dbContext = new Context())
            {
                students = dbContext.StudentsDbSet
                    .Where(s => s.Pesel.Equals(pesel))
                    .ToList();
            }
            return students;
        }

        public int CountStudents()
        {
            using (var dbContext = new Context())
            {
                return dbContext.StudentsDbSet.Count();
            }
        }

        public Student GetStudentByPesel(string pesel)
        {
            using (var dbContext = new Context())
            {
                try
                {
                    var student = dbContext.StudentsDbSet
                        .Include(s => s.Courses)
                        .First(s => s.Pesel == pesel);
                    return student;
                }
                catch (Exception)
                {
                    return null;
                }               
            }
        }
    }
}
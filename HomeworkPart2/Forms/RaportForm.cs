﻿using System;
using CourseLogicLayer;
using CourseLogicLayer.Dtos;

namespace HomeworkPart2.Forms
{
    public class RaportForm
    {
        public delegate void RaportEventHandler(object sender, EventArgs e);
        public event RaportEventHandler RaportCreated;

        private string WriteLine(string text)
        {
            return "" + text + "\n";
        }

        private void OnRaportCreated()
        {
            RaportCreated?.Invoke(this, EventArgs.Empty);
        }

        public string ShowRaport(CourseDto course)
        {
            string outputText = "";
            outputText += WriteLine(course.CourseName);
            outputText += WriteLine("" + course.CourseStartDate);
            outputText += WriteLine("" + course.PresenceThreshold);
            outputText += WriteLine("" + course.HomeworkThreshold);

            int maxPoints = 0;
            foreach (var homework in course.Homeworks)
            {
                maxPoints += homework.RequiredPoints;
            }

            outputText += WriteLine("Lista obecności");

            foreach (var student in course.Students)
            {
                int studentPersinstece = course.StudentPresence(student);
                float percentages = CalculatingManagment.PercentageCalculation(studentPersinstece, course.ClassDays.Count);

                if (percentages >= course.PresenceThreshold)
                    outputText += WriteLine(student.Pesel + " " + student.Name + " " + student.Surname + " " +
                                      studentPersinstece + "/" + course.ClassDays.Count + " (" +
                                      percentages + "%) - Zaliczone");
                else if (percentages < course.PresenceThreshold)
                    outputText += WriteLine(student.Pesel + " " + student.Name + " " + student.Surname + " " +
                                      studentPersinstece + "/" + course.ClassDays.Count + " (" +
                                      percentages + "%) - Niezaliczone");
                else
                    outputText += WriteLine("Brak danych do wyswietlenia");
            }

            outputText += WriteLine("Raport z prac domowych");
            foreach (var student in course.Students)
            {
                var studentPoints = course.StudentPoints(student);
                float percentages = CalculatingManagment.PercentageCalculation(studentPoints, maxPoints);

                if (percentages >= course.HomeworkThreshold)
                    outputText += WriteLine(student.Pesel + " " + student.Name + " " + student.Surname + " " +
                                      studentPoints + "/" + maxPoints + " (" +
                                      percentages + "%) - Zaliczone");
                else if (percentages < course.HomeworkThreshold)
                    outputText += WriteLine(student.Pesel + " " + student.Name + " " + student.Surname + " " +
                                      studentPoints + "/" + maxPoints + " (" +
                                      percentages + "%) - Niezaliczone");
                else
                    outputText += WriteLine("Brak danych do wyswietlenia");
            }
            OnRaportCreated();
            return outputText;
        }
    }
}
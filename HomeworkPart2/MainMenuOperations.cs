﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using CourseLogicLayer.Dtos;
using Homework1;
using Homework1.Services;
using HomeworkPart2.Forms;

namespace HomeworkPart2
{
    public class MainMenuOperations
    {
        private static bool _isProgramWorking = true;
        public delegate void DelegateAddStudentOperation();
        public delegate void DelegateUpdateStudentOperation();
        //public delegate void DelegateCourseOperationMenu();
        public delegate void DelegateAddCourseOperation();
        public delegate void DelegateSelectCourseOperation();
        public delegate void DelegateExit();


        public Dictionary<string, Delegate> MenuDict = new Dictionary<string, Delegate>
        {
            { "AS", new DelegateAddStudentOperation(AddStudentOperation)},
            { "US", new DelegateUpdateStudentOperation(UpdateStudentOperation)},
            { "AC", new DelegateAddCourseOperation(AddCourseOperation)},
            { "SC", new DelegateSelectCourseOperation(CheckCourse)},
            { "E", new DelegateExit(Exit)}
        };


        public void MainMenu()
        {
            while (_isProgramWorking)
            {
                Console.WriteLine("Dodaj studenta - AS" +
                                  "\nZmien dane Studenta - US" +
                                  "\nDodaj kurs - AC " +
                                  "\nWybierz istniejacy kurs - SC " +
                                  "\nWyjscie z programu - E"
                                  );
                string operation = Console.ReadLine();

                foreach (var option in MenuDict)
                {
                    if (operation == option.Key)
                        MenuDict[operation].DynamicInvoke();
                    else
                        Console.WriteLine("Bledna komenda menu");
                }
 
            } 
        }

        private static void Exit()
        {
            _isProgramWorking = false;
        }
        private static void CheckCourse()
        {
            CourseDto course = SelectCourseOperation();
            if (course != null)
            {
                CourseOperationMenu(course);
            }
        }
        private static void UpdateStudentOperation()
        {
            var pesel = ConsoleInput.GetPesel("Podaj pesel studenta");
            var service = new StudentService();
            var student = service.GetStudentDtoByPesel(pesel);
            if (student == null)
            {
                Console.WriteLine("Nie znalazlem takiego studenta.");
                return;
            }

            Console.WriteLine("Podaj nowe imie studenta");
            var input = Console.ReadLine();
            if (input.Length > 0)
            {
                student.Name = input;
            }

            Console.WriteLine("Podaj nowe nazwisko studenta");
            input = Console.ReadLine();
            if (input.Length > 0)
            {
                student.Surname = input;
            }
                
            Console.WriteLine("Podaj nowa date urodzin studenta");
            input = Console.ReadLine();
            if (input.Length > 0)
            {
                try
                {
                    var datetime = DateTime.Parse(input);
                    student.Birthday = datetime;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Zły format daty. Data nie zostanie zmodyfikowana..");
                }
            }

            string removeFromCourse = ConsoleInput.GetYesNo("Czy chcesz wypisac tego kursanta z kursu? (tak/nie)");
            if (removeFromCourse == "tak")
            {
                foreach (var course in student.Courses)
                {
                    Console.WriteLine(course.Id+". "+course.CourseName+" ("+course.TrainerName+")");
                }
                var courseId = ConsoleInput.GetInt("Podaj identyfikator kursu");
                var selectedCourseDto = student.Courses.First(c => c.Id == courseId) ?? null;
                var studentService = new StudentService();
                var isRemoved = studentService.RemoveStudentFromCourse(student, selectedCourseDto);
                if (isRemoved)
                {
                    Console.WriteLine("ok");
                }
                else
                {
                    Console.WriteLine("Kursant nie uczescza na kurs o podanym indentyfikatorze");
                    Console.WriteLine("Operacja zakonczona niepowodzeniem");
                }
            }

            service.UpdateStudent(student);
        }

        private static void CourseOperationMenu(CourseDto course)
        {
            var operation = new CourseOperationMenu(course);
            operation.ShowMenu();
        }

        private static CourseDto SelectCourseOperation()
        {
            try
            {
                var courseService = new CourseService();
                List<CourseDto> list = courseService.GetAllCourses();
                Console.WriteLine("Wybierz kurs:");
                for (int i = 0; i < list.Count; i++)
                {
                    var course = list[i];
                    Console.WriteLine(i + ". " + course.CourseName + " (" + course.TrainerName + ")");
                }
                int courseIndex = ConsoleInput.GetInt("Podaj numer kursu");
                Console.WriteLine("Wybrano kurs" + list[courseIndex].CourseName);
                return list[courseIndex];
            }
            catch (Exception e)
            {
                return null;
            }
         }

        private static void AddCourseOperation()
        {
            StudentService ss = new StudentService();
            if (ss.CountStudents() == 0)
            {
                Console.WriteLine("Najpierw dodaj studentow!");
                return;
            }

            CourseDto courseDto = new CourseDto();

            Console.WriteLine("Dziennik kursu, wprowadź dane: ");
            courseDto.CourseName = ConsoleInput.GetCourseName("Podaj nazwę kursu");

            Console.WriteLine("Podaj imię oraz nazwisko Prowadzącego");
            courseDto.TrainerName = Console.ReadLine();

            courseDto.CourseStartDate = ConsoleInput.GetDateTime("Podaj date rozpoczęcia kursu [dd/mm/yyyy]");

            courseDto.HomeworkThreshold =
                ConsoleInput.GetInt("Podaj próg punktowy zaliczenia prac domowych w procentach (bez wpisywania %)");

            courseDto.PresenceThreshold =
                ConsoleInput.GetInt("Podaj próg punktowy zaliczenia obecnosci (bez wpisywania %)");

            courseDto.AmountOfStudents = ConsoleInput.GetInt("Podaj liczbę kursantow");

            try
            {
                var cs = new CourseService();
                cs.AddCourse(courseDto);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private static void AddStudentOperation()
        {
            try
            {
                StudentDto s = NewStudentForm.ShowForm();
                var studentService = new StudentService();
                studentService.AddStudent(s);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
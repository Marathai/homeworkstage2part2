﻿using Homework1;

namespace HomeworkPart2
{
    class Program
    {
        static void Main(string[] args)
        {
            var mainMenuLogic = new MainMenuOperations();
            mainMenuLogic.MainMenu();
        }
    }
}

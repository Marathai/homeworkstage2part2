﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CourseLogicLayer;

namespace Homework1
{
    public class ConsoleInput
    {

        private static void showMessage(string message)
        {
            Console.WriteLine(message);
        }

        private static void showError(string message = "")
        {
            Console.WriteLine("Cos poszlo nie tak. " + message);
        }


        public static string GetCourseName(string message)
        {
            string courseName;
            string pattern = @"^C#_([a-zA-Z0-9_]+)_([A-Z]{2})$";
            while (true)
            {
                showMessage(message);
                courseName = Console.ReadLine();
                try
                {
                    var regex = new Regex(pattern);
                    if(regex.IsMatch(courseName))
                        return courseName;
                }
                catch (Exception)
                {
                    showError();
                }
                showError();
            }
        }

        public static DateTime GetDateTime(string message)
        {
            DateTime date;
            while (true)
            {
                showMessage(message);
                try
                {
                    date = DateTime.Parse(Console.ReadLine());
                    return date;
                }
                catch (Exception e)
                {
                    showError();
                }
            }
        }


        public static int GetInt(string message)
        {
            int getInt;
            while (true)
            {
                showMessage(message);
                try
                {
                    getInt = Int32.Parse(Console.ReadLine());
                    return getInt;
                }
                catch (Exception e)
                {
                    showError();
                }
            }
        }

        public static object GetEnum(Type type, string message)
        {
            while (true)
            {
                showMessage(message);
                try
                {
                    var enumType = Enum.Parse(type, Console.ReadLine());
                    return enumType;
                }
                catch (Exception e)
                {
                    showError();
                }
            }
        }

        public static string GetYesNo(string message)
        {
            while (true)
            {
                showMessage(message);
                var answer = Console.ReadLine();
                if (answer == "tak")
                {
                    return answer;
                }
                if (answer == "nie")
                {
                    return answer;
                }
                showError();
            }
        }

        public static Pesel GetPesel(string message)
        {
            while (true)
            {
                showMessage(message);
                var pesel = Console.ReadLine();
                try
                {
                    Pesel newPesel = new Pesel(pesel);
                    return newPesel;
                }
                catch (Exception)
                {
                    showError();
                }
                showError();
            }
        }
    }
}
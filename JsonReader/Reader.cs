﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;

namespace JsonReader
{
    public class Reader
    {
        private string path;
        private string data;
        public void Program()
        {
            // JObject jObject;
            string json;
            while (true)
            {
                Console.WriteLine("Podaj sciezke pliku");
                path = Console.ReadLine();
                try
                {
                    json = File.ReadAllText(path);
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Bledna sciezka");
                }
            }

            dynamic jObject = JsonConvert.DeserializeObject(json);


            Console.WriteLine("Imie: " + jObject.Name);
            Console.WriteLine("Imie prowadzacego: " + jObject.TrainerName);
            Console.WriteLine("Średni procent z prac domowych " + jObject["HomeworkPercentage"]);
            Console.WriteLine("Średni procent z obecności " + jObject["PresencePercentage"]);
            Console.WriteLine("Ile osób zaliczyło prace domowe " + jObject["PplWhoPassHomeworks"]);
            Console.WriteLine("Ile osób zaliczyło obecność " + jObject["PplWhoPassPresence"]);
        }
    }
}
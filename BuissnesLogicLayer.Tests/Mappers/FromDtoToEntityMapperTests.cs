﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Homework1.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CourseLogicLayer;
using CourseLogicLayer.Dtos;
using DataLayer.Models;

namespace Homework1.Mappers.Tests
{
    [TestClass()]
    public class FromDtoToEntityMapperTests
    {
        [TestMethod()]
        public void StudentDtoToEntityTest()
        {
            var studentDto = new StudentDto();
            var pesel = new Pesel("12345123450");
            studentDto.Pesel = pesel;
            studentDto.Id = 1;
            studentDto.Birthday = DateTime.Parse("1/1/1");
            studentDto.StudentGender = StudentDto.Gender.Male;
            studentDto.Name = "Cainon";
            studentDto.Surname = "Dziwny";

            var expectedEntity = new Student();
            expectedEntity.Pesel = "12345123450";
            expectedEntity.Id = 1;
            expectedEntity.Birthday = DateTime.Parse("1/1/1");
            expectedEntity.StudentGender = Student.Gender.Male;
            expectedEntity.Name = "Cainon";
            expectedEntity.Surname = "Dziwny";

            var result = FromDtoToEntityMapper.StudentDtoToEntity(studentDto);
            Assert.AreEqual(expectedEntity.Pesel, result.Pesel);
            Assert.AreEqual(expectedEntity.Id, result.Id);
            Assert.AreEqual(expectedEntity.Birthday, result.Birthday);
            Assert.AreEqual(expectedEntity.StudentGender, result.StudentGender);
            Assert.AreEqual(expectedEntity.Name, result.Name);
            Assert.AreEqual(expectedEntity.Surname, result.Surname);
        }

        [TestMethod()]
        public void StudentDtoToEntityReturnsNullTest()
        {
            Assert.AreEqual(null, FromDtoToEntityMapper.StudentDtoToEntity(null));
        }



        [TestMethod()]
        public void CourseDtoToEntityTest()
        {
            var studentDto = new StudentDto();
            var pesel = new Pesel("12345123450");
            studentDto.Pesel = pesel;
            studentDto.Id = 1;
            studentDto.Birthday = DateTime.Parse("1/1/1");
            studentDto.StudentGender = StudentDto.Gender.Male;
            studentDto.Name = "Cainon";
            studentDto.Surname = "Dziwny";

            var homework1 = new HomeworkDto();
            homework1.Id = 1;
            homework1.RequiredPoints = 12;
            homework1.EarnedPoints = new Dictionary<StudentDto, int>{{studentDto, 2}};

            var classDay1 = new ClassDayDto();
            classDay1.Id = 1;
            classDay1.ClassDate = DateTime.Parse("2/3/2");
            classDay1.Presense = new Dictionary<StudentDto, PresenceDto> { { studentDto, new PresenceDto(classDay1, studentDto, true) } };


            var courseDto = new CourseDto();
            courseDto.Id = 2;
            courseDto.AmountOfCourseDays = 11;
            courseDto.AmountOfStudents = 4;
            courseDto.CourseName = "Wazny kurs";
            courseDto.CourseStartDate = DateTime.Parse("2/2/2");
            courseDto.Students = new List<StudentDto> { studentDto };
            courseDto.Homeworks = new List<HomeworkDto> { homework1 };
            courseDto.ClassDays = new List<ClassDayDto> { classDay1 };


            var student = new Student();
            student.Pesel = "12345123450";
            student.Id = 1;
            student.Birthday = DateTime.Parse("1/1/1");
            student.StudentGender = Student.Gender.Male;
            student.Name = "Cainon";
            student.Surname = "Dziwny";

            var homework = new Homework();
            homework.Id = 1;
            homework.RequiredPoints = 12;
            homework.EarnedPoints = new List<EarnedPoints>{{new EarnedPoints(homework, student, 2)}};

            var classDay = new ClassDay();
            classDay.Id = 1;
            classDay.ClassDate = DateTime.Parse("2/3/2");
            classDay.Presense = new List<Presence>(); {
                {
                    new Presence(classDay, student, true);
                } };

            var expectedEntity = new Course();
            expectedEntity.Id = 2;
            expectedEntity.AmountOfCourseDays = 11;
            expectedEntity.AmountOfStudents = 4;
            expectedEntity.CourseName = "Wazny kurs";
            expectedEntity.CourseStartDate = DateTime.Parse("2/2/2");
            expectedEntity.Students = new List<Student> { student };
            expectedEntity.Homeworks = new List<Homework> { homework };
            expectedEntity.ClassDays = new List<ClassDay> { classDay };

            var result = FromDtoToEntityMapper.CourseDtoToEntity(courseDto);
            Assert.AreEqual(expectedEntity.Id, result.Id);
            Assert.AreEqual(expectedEntity.AmountOfCourseDays, result.AmountOfCourseDays);
            Assert.AreEqual(expectedEntity.AmountOfStudents, result.AmountOfStudents);
            Assert.AreEqual(expectedEntity.CourseName, result.CourseName);
            Assert.AreEqual(expectedEntity.CourseStartDate, result.CourseStartDate);
            Assert.AreEqual(expectedEntity.Students.Count, result.Students.Count);
            Assert.AreEqual(expectedEntity.Homeworks.Count, result.Homeworks.Count);
            Assert.AreEqual(expectedEntity.ClassDays.Count, result.ClassDays.Count);

        }

        [TestMethod()]
        public void CourseDtoToEntityReturnsNullTest()
        {
            Assert.AreEqual(null, FromDtoToEntityMapper.CourseDtoToEntity(null));
        }

        [TestMethod()]
        public void ClassDayDtoToEntityWithNullTest()
        {
            Assert.AreEqual(null, FromDtoToEntityMapper.ClassDayDtoToEntity(null));
        }

        [TestMethod()]
        public void HomeworkDtoToEntityWithNullTest()
        {
            Assert.AreEqual(null, FromDtoToEntityMapper.HomeworkDtoToEntity(null));
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Mapping;
using System.Linq;
using CourseLogicLayer;
using CourseLogicLayer.Dtos;
using DataLayer.Repositories;
using Homework1.Mappers;

namespace Homework1.Services
{
    public class CourseService
    {
        private CourseDto _currentCourseDto = null;

        public void AddCourse(CourseDto courseDto)
        {
            var course = FromDtoToEntityMapper.CourseDtoToEntity(courseDto);
            var courseRepo = new CourseRepository();
            courseRepo.AddElement(course);
        }

        public void UpdateCourse(CourseDto courseDto)
        {
            var course = FromDtoToEntityMapper.CourseDtoToEntity(courseDto);
            var repo = new CourseRepository();
            repo.UpdateCourse(course);
        }

        public List<CourseDto> GetAllCourses()
        {
            var repo = new CourseRepository();

            return repo
                .GetCourses()?
                .Select(FromEntityToDtoMapper.EntityToCourseDto)
                .ToList();
        }

        public void SetCurrentCourseByIndex(int index)
        {
            if (index < 0)
            {
                throw new Exception("Indeks nie moze byc liczba ujemna!");
            }

            var courses = GetAllCourses();
            if (courses.Count <= index)
            {
                throw new Exception("Nie ma takiego kursu!");
            }

            _currentCourseDto = courses[index];
        }

        public CourseDto GetCurrentCourseDto()
        {
            return _currentCourseDto;
        }

        public bool AddStudentToCourse(StudentDto studentDto, CourseDto courseDto)
        {
            var repo = new CourseRepository();
            if (!repo.AddStudentToCourse(studentDto.Pesel.ToString(), courseDto.Id))
            {
                Console.WriteLine("Student o podanym peselu juz jest na kursie");
                return false;
            }
            return true;
        }

        public bool AddHomeworkToCourse(HomeworkDto homeworkDto, CourseDto courseDto)
        {
            var repo = new CourseRepository();
            if (!repo.AddHomeworkToCourse(FromDtoToEntityMapper.HomeworkDtoToEntity(homeworkDto), courseDto.Id))
            {
                return false;
            }
            return true;

        }

        public void RemoveStudentFromCourse(StudentDto studentDto, CourseDto courseDto)
        {
            var repo = new CourseRepository();
            if (!repo.RemoveStudentFromCourse(studentDto.Id, courseDto.Id))
                Console.WriteLine("Student o podanym peselu nie jest na tym kursie");
            else
                Console.WriteLine("ok");
        }

        public bool CreateExampleCourses()
        {
            var pesel1 = new Pesel("12345123450");
            var student1 = new StudentDto();
            student1.Pesel = pesel1;
            student1.Id = 1;
            student1.Birthday = DateTime.Parse("1/1/1");
            student1.Name = "Janusz";
            student1.Surname = "Nowak";
            student1.StudentGender = StudentDto.Gender.Male;

            var pesel2 = new Pesel("12345123451");
            var student2 = new StudentDto();
            student2.Pesel = pesel2;
            student2.Id = 2;
            student2.Birthday = DateTime.Parse("1/1/1");
            student2.Name = "Halyna";
            student2.Surname = "Nowak";
            student2.StudentGender = StudentDto.Gender.Female;

            var pesel3 = new Pesel("12345612345");
            var student3 = new StudentDto();
            student3.Pesel = pesel3;
            student3.Id = 3;
            student3.Birthday = DateTime.Parse("1/1/2");
            student3.Name = "Brajan";
            student3.Surname = "Pasek";
            student3.StudentGender = StudentDto.Gender.Other;

            var pesel4 = new Pesel("12345654321");
            var student4 = new StudentDto();
            student4.Pesel = pesel4;
            student4.Id = 4;
            student4.Birthday = DateTime.Parse("1/2/2");
            student4.Name = "Karyna";
            student4.Surname = "Ajfon";
            student4.StudentGender = StudentDto.Gender.Female;

            var homework1 = new HomeworkDto();
            homework1.Id = 1;
            homework1.RequiredPoints = 12;
            homework1.EarnedPoints = new Dictionary<StudentDto, int> {{student1,2},{student2,11} };

            var homework2 = new HomeworkDto();
            homework2.Id = 2;
            homework2.RequiredPoints = 12;
            homework2.EarnedPoints = new Dictionary<StudentDto, int> { { student1, 5 }, { student2, 10 } };

            var homework3 = new HomeworkDto();
            homework3.Id = 3;
            homework3.RequiredPoints = 15;
            homework3.EarnedPoints = new Dictionary<StudentDto, int> { { student1, 1}, { student2, 12 } };

            var classDay1 = new ClassDayDto();
            classDay1.Id = 1;
            classDay1.ClassDate = DateTime.Parse("2/3/2");
            classDay1.Presense = new Dictionary<StudentDto, PresenceDto> { {student1,new PresenceDto(classDay1, student1, true) }, { student2, new PresenceDto(classDay1, student2, true) } };
            var classDay2 = new ClassDayDto();
            classDay2.Id = 2;
            classDay2.ClassDate = DateTime.Parse("2/4/2");
            classDay2.Presense = new Dictionary<StudentDto, PresenceDto> { { student1, new PresenceDto(classDay2, student1, false) }, { student2, new PresenceDto(classDay2, student2, true) } };

            var classDay3 = new ClassDayDto();
            classDay3.Id = 1;
            classDay3.ClassDate = DateTime.Parse("2/3/2");
            classDay3.Presense = new Dictionary<StudentDto, PresenceDto> { { student1, new PresenceDto(classDay3, student1, false) }, { student2, new PresenceDto(classDay3, student2, true) } };

            var course = new CourseDto();
            course.Id = 1;
            course.AmountOfCourseDays = 12;
            course.AmountOfStudents = 2;
            course.CourseName = "Kurs na superbohatera";
            course.CourseStartDate = DateTime.Parse("2/1/2");
            course.Students = new List<StudentDto> {student1, student2};
            course.HomeworkThreshold = 50;
            course.PresenceThreshold = 50;
            course.Homeworks = new List<HomeworkDto> {homework1, homework2, homework3};
            course.ClassDays = new List<ClassDayDto> {classDay1, classDay2, classDay3};

            AddCourse(course);

            return true;
        }

        public bool AddClassDayToCourse(ClassDayDto classDay, CourseDto courseDto)
        {
            var repo = new CourseRepository();
            if (!repo.AddClassDayToCourse(FromDtoToEntityMapper.ClassDayDtoToEntity(classDay), courseDto.Id))
            {
                return false;
            }
            return true;
        }
    }
}
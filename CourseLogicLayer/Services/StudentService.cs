﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLayer.Repositories;
using Homework1.Dtos;
using Homework1.Mappers;

namespace Homework1.Services
{
    public class StudentService
    {
        private bool CheckStudentExistsByPesel(Pesel pesel)
        {
            var studentRepo = new StudentRepository();
            Console.WriteLine("Sprawdzam, czy student o peselu "+pesel+" istnieje.");
            var students = studentRepo.GetStudentsByPesel(pesel.ToString());
            return !(students == null || students.Count == 0);
        }

        public void AddStudent(StudentDto studentDto)
        {
            if (CheckStudentExistsByPesel(studentDto.Pesel))
            {
                throw new Exception("Student o takim peselu juz instnieje!");
            }

            var student = FromDtoToEntityMapper.StudentDtoToEntity(studentDto);
            var studentRepo = new StudentRepository();
            studentRepo.AddStudent(student);
        }

        public int CountStudents()
        {
            var repo = new StudentRepository();
            return repo.CountStudents();
        }

        public StudentDto GetStudentDtoByPesel(Pesel pesel)
        {
            var repo = new StudentRepository();
            return FromEntityToDtoMapper.EntityModelToStudentDto(
                repo.GetStudentByPesel(pesel.ToString())
            );
        }
    }
}
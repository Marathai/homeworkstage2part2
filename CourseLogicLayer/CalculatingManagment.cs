﻿using System;

namespace CourseLogicLayer
{
    public class CalculatingManagment
    {
        public static float PercentageCalculation(int firstArg, int secondArg)
        {
            float result;
            try
            {
                if (secondArg == 0) throw new DivideByZeroException();
                result = (firstArg / (float) secondArg) * 100;

                return result;
            }
            catch (DivideByZeroException e)
            {
                return 0;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;

namespace CourseLogicLayer.Dtos
{
    public class ClassDayDto
    {
        public int Id;
        public DateTime ClassDate;
        public Dictionary<StudentDto, PresenceDto> Presense= new Dictionary<StudentDto, PresenceDto>();

    }
}

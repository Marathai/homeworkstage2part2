﻿using System;
using System.Collections.Generic;
using System.Linq;
using CourseLogicLayer.Dtos;
using DataLayer.Models;

namespace Homework1.Mappers
{
    public class FromDtoToEntityMapper
    {
        public static Student StudentDtoToEntity(StudentDto studentDto)
        {
            if (studentDto == null)
                return null;

            var student = new Student();
            student.Id = studentDto.Id;
            student.Pesel = studentDto.Pesel.ToString();
            student.Birthday = studentDto.Birthday;
            student.Name = studentDto.Name;
            student.StudentGender = (Student.Gender)studentDto.StudentGender;
            student.Surname = studentDto.Surname;

            return student;
        }

        public static Course CourseDtoToEntity(CourseDto courseDto)
        {
            if (courseDto == null)
                return null;

            var course = new Course();
            course.Id = courseDto.Id;
            course.AmountOfCourseDays = courseDto.AmountOfCourseDays;
            course.AmountOfStudents = courseDto.AmountOfStudents;
            course.CourseName = courseDto.CourseName;
            course.CourseStartDate = courseDto.CourseStartDate;
            course.HomeworkThreshold = courseDto.HomeworkThreshold;
            course.PresenceThreshold = courseDto.PresenceThreshold;
            course.Students = courseDto.Students?.Select(s => StudentDtoToEntity(s)).ToList();
            course.Homeworks = courseDto.Homeworks?.Select(s => HomeworkDtoToEntity(s)).ToList();
            course.ClassDays = courseDto.ClassDays?.Select(c => ClassDayDtoToEntity(c)).ToList();
            return course;
        }

        public static Homework HomeworkDtoToEntity(HomeworkDto homeworkDto)
        {
            if (homeworkDto == null)
                return null;

            var homework = new Homework();
            homework.Id = homeworkDto.Id;
            homework.RequiredPoints = homeworkDto.RequiredPoints;
            foreach (var homeworkDtoEarnedPoint in homeworkDto.EarnedPoints)
            {
                homework.EarnedPoints.Add(
                    new EarnedPoints(
                        homework, 
                        StudentDtoToEntity(homeworkDtoEarnedPoint.Key), 
                        homeworkDtoEarnedPoint.Value)
                );
            }
            return homework;
        }

        public static ClassDay ClassDayDtoToEntity(ClassDayDto classDayDto)
        {
            if (classDayDto == null)
                return null;

            var classDay = new ClassDay();
            classDay.Id = classDayDto.Id;
            classDay.ClassDate = classDayDto.ClassDate;
            foreach (var presence in classDayDto.Presense)
            {
                classDay.Presense.Add(
                    new Presence(
                        classDay, 
                        StudentDtoToEntity(presence.Key), 
                        presence.Value.isPresent
                    )
                );
            }
            return classDay;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using CourseLogicLayer;
using CourseLogicLayer.Dtos;
using DataLayer.Models;

namespace Homework1.Mappers
{
    public class FromEntityToDtoMapper
    {
        public static StudentDto EntityModelToStudentDtoSimple(Student student)
        {
            if (student == null)
                return null;
            var studentDto = new StudentDto();
            studentDto.Id = student.Id;
            studentDto.Pesel = new Pesel(student.Pesel);
            studentDto.Birthday = student.Birthday;
            studentDto.Name = student.Name;
            studentDto.StudentGender = (StudentDto.Gender)student.StudentGender;
            studentDto.Surname = student.Surname;
            return studentDto;
        }

        public static StudentDto EntityModelToStudentDto(Student student)
        {
            if (student == null)
                return null;
            var studentDto = EntityModelToStudentDtoSimple(student);
            try
            {

                studentDto.Courses = student.Courses?.Select(EntityToCourseDtoSimple).ToList()
                                     ?? new List<CourseDto>();
            }
            catch (Exception){}
            return studentDto;
        }

        public static CourseDto EntityToCourseDtoSimple(Course course)
        {
            if (course == null)
                return null;

            var courseDto = new CourseDto();
            courseDto.Id = course.Id;
            courseDto.AmountOfCourseDays = course.AmountOfCourseDays;
            courseDto.AmountOfStudents = course.AmountOfStudents;
            courseDto.HomeworkThreshold = course.HomeworkThreshold;
            courseDto.PresenceThreshold = course.PresenceThreshold;
            courseDto.CourseName = course.CourseName;
            courseDto.CourseStartDate = course.CourseStartDate;
            return courseDto;
        }

        public static CourseDto EntityToCourseDto(Course course)
        {
            if (course == null)
                return null;
            var courseDto = EntityToCourseDtoSimple(course);

                courseDto.Students = course.Students?.Select(EntityModelToStudentDtoSimple).ToList()
                                     ?? new List<StudentDto>();
            try
            {
                courseDto.Homeworks = course.Homeworks?.Select(EntityToHomeworkDto).ToList()
                                      ?? new List<HomeworkDto>();
                courseDto.ClassDays = course.ClassDays?.Select(EntityToClassDayDto).ToList()
                                      ?? new List<ClassDayDto>(courseDto.AmountOfCourseDays);
                return courseDto;
            }
            catch (Exception e)
            {

                //Console.WriteLine(e);
                //Tu wywala dziwny exeption ktorego nie umiem obsluzyc.
            }
            return courseDto;
        }

        public static HomeworkDto EntityToHomeworkDto(Homework homework)
        {
            if (homework == null)
                return null;

            var homeworkDto = new HomeworkDto(homework.RequiredPoints);
            homeworkDto.Id = homework.Id;
            foreach (var homeworkDtoEarnedPoint in homework.EarnedPoints)
            {
                homeworkDto.EarnedPoints.Add(
                    EntityModelToStudentDto(homeworkDtoEarnedPoint.student),
                    homeworkDtoEarnedPoint.earnedPoints
                );
            }
            return homeworkDto;
        }

        public static ClassDayDto EntityToClassDayDto(ClassDay classDay)
        {
            if (classDay == null)
                return null;

            var classDaykDto = new ClassDayDto();
            classDaykDto.Id = classDay.Id;
            classDaykDto.ClassDate = classDay.ClassDate;
            foreach (var p in classDay.Presense)
            {
                classDaykDto.Presense.Add(
                    EntityModelToStudentDtoSimple(p.student),
                    EntityToPresenceDto(p)
                );
            }
            return classDaykDto;
        }

        public static PresenceDto EntityToPresenceDto(Presence p)
        {
            return new PresenceDto(p.Id,
                EntityToClassDayDto(p.classDay),
                EntityModelToStudentDtoSimple(p.student),
                p.isPresent
            );
        }
    }
}